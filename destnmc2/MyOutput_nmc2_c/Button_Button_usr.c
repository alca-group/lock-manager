/*2024-06-07T13:22:33.422206259Z*/

/**********************************************************************
 * @file Button_Button_usr.c
 *
 * Automatically generated by Software Production Engineer
 * Statemachine Code Generator @version v1.0.0
 * @generated_from _2022x_1ad00ce_1659686618091_973487_3644
 **********************************************************************/

#ifdef STATEMACHINE

/**********************************************************************
 * handy helpers
 **********************************************************************/
#define control         instance->control_snd
#define pin             instance->pin_io
#define pinData         instance->classifier->pinData

/* BEG_USER_CODE(header) do not remove */

/* user-modifiable header part */

/* END_USER_CODE(header) do not remove */

/**********************************************************************
 * Function on_Effect
 * Implements Effect of Transition between States: on -> off
 * Effect calls Activity: Send Off
 *
 * @generated_from _2022x_1ad00ce_1659687813473_879953_3794
 * @generated_from _2022x_1ad00ce_1659691265299_712196_4743
 **********************************************************************/
static void on_Effect(Button_struct *instance) {
    /* BEG_USER_CODE(Effect_2022x_1ad00ce_1659687813473_879953_3794) do not remove */

    (void)instance; /* MISRA rule 2.7 (usused parameter) */
    instance->DONE_ACTIVITY = _basic_Button_Send_Off(&(instance->on_Effect_Button_Send_Off));

    /* END_USER_CODE(Effect_2022x_1ad00ce_1659687813473_879953_3794) do not remove */
}

/**********************************************************************
 * Function on_Entry
 * Implements "Entry" of State: on
 * Entry calls Activity: Send On
 *
 * @generated_from _2022x_1ad00ce_1659687553559_425198_3695
 * @generated_from _2022x_1ad00ce_1659691200449_687228_4664
 **********************************************************************/
static void on_Entry(Button_struct *instance) {
    /* BEG_USER_CODE(Entry_2022x_1ad00ce_1659687553559_425198_3695) do not remove */

    (void)instance; /* MISRA rule 2.7 (usused parameter) */
    instance->DONE_ACTIVITY = _basic_Button_Send_On(&(instance->on_Entry_Button_Send_On));

    /* END_USER_CODE(Entry_2022x_1ad00ce_1659687553559_425198_3695) do not remove */
}

static void StateMachine_init(Button_struct *instance) {

    /* BEG_USER_CODE(init_state_machine) do not remove */

    /* do whatever needed */
    (void)instance; /* MISRA rule 2.7 (usused parameter) */

    /* END_USER_CODE(init_state_machine) do not remove */
}

static void StateMachine_end(Button_struct *instance) {
    /* BEG_USER_CODE(end_state_machine) do not remove */

    /* do whatever needed */
    _basic_Button_Send_Off_end(&(instance->on_Effect_Button_Send_Off));
    _basic_Button_Send_On_end(&(instance->on_Entry_Button_Send_On));

    /* END_USER_CODE(end_state_machine) do not remove */
}

/* BEG_USER_CODE(footer) do not remove */

/* user-modifiable footer part */

/* END_USER_CODE(footer) do not remove */

#endif /* STATEMACHINE */
/* end of file */
