/* @date 2024-06-04T08:56:16.852328891Z */

/**********************************************************************
 * @file Cse_LockManagerClass_hook.h
 *
 * Automatically generated by Software Producer Engineer
 * Runtime code generator @version 1.0.0
 **********************************************************************/


#ifndef CSE_LOCKMANAGERCLASS_HOOK_H
#define CSE_LOCKMANAGERCLASS_HOOK_H

#include "Cse_LockManagerClass_hook_cfg.h"

#if defined(Cse_ReadHook_LockManagerClass_input_lockControl)
#undef ReadHook_LockManagerClass_input_lockControl
extern void Cse_ReadHook_LockManagerClass_input_lockControl_Start(ControlEnum* from_UNCONNECTED_input_lockControl);
extern void Cse_ReadHook_LockManagerClass_input_lockControl_Return(ControlEnum* from_UNCONNECTED_input_lockControl);
#else
#define Cse_ReadHook_LockManagerClass_input_lockControl_Start(value)  ((void) (0))
#define Cse_ReadHook_LockManagerClass_input_lockControl_Return(value)  ((void) (0))
#endif /* ReadHook_LockManagerClass_input_lockControl */


#if defined(Cse_WriteHook_LockManagerClass_output_lockStatus)
#undef WriteHook_LockManagerClass_output_lockStatus
extern void Cse_WriteHook_LockManagerClass_output_lockStatus_Start(LockEnum* from_UNCONNECTED_output_lockStatus);
extern void Cse_WriteHook_LockManagerClass_output_lockStatus_Return(LockEnum* from_UNCONNECTED_output_lockStatus);
#else
#define Cse_WriteHook_LockManagerClass_output_lockStatus_Start(value)  ((void) (0))
#define Cse_WriteHook_LockManagerClass_output_lockStatus_Return(value)  ((void) (0))
#endif /* WriteHook_LockManagerClass_output_lockStatus */

#endif /* CSE_HOOK_H */

/* end of file */
