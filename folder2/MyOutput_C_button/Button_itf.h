/* @date 2024-06-10T06:27:23.255596811Z */

/**********************************************************************
 * @file Button_itf.h
 *
 * Automatically generated by Software Producer Engineer
 * Runtime code generator @version 
 * @generated_from _2022x_1ad00ce_1659678969433_444652_3277
 **********************************************************************/

#ifndef CSE_Button_ITF_H
#define CSE_Button_ITF_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Includes -------------------------------------*/
#include "Button_type.h"

#include "Cse_cfg.h" /* define NB_INST_BUTTON */

/* CSE Component Structure declaration ----------*/
typedef struct{
	/* Write functions */
	/* @generated_from _2022x_1ad00ce_1659683574035_551596_3462 */
	/* @generated_from _2022x_1_1ad00ce_1677485612205_746538_3821 */	
	void (*Write_control_snd)(Boolean* msg);
	/* Trigger functions */
	/* @generated_from _2022x_1ad00ce_1659683574035_551596_3462 */
	/* @generated_from Event_UUID__2022x_1_1ad00ce_1677485337980_615644_3467__2022x_1ad00ce_1659688230703_340985_4140 */  
	void (*Trigger_control_On)(void);
	/* @generated_from _2022x_1ad00ce_1659683574035_551596_3462 */
	/* @generated_from Event_UUID__2022x_1_1ad00ce_1677485337980_615644_3467__2022x_1ad00ce_1659688235695_364403_4143 */  
	void (*Trigger_control_Off)(void);
	/* @generated_from _2022x_1ad00ce_1659683574035_551596_3462 */
	/* @generated_from Event_UUID__2022x_1_1ad00ce_1677485337980_615644_3467__2022x_1ad00ce_1659688287223_365621_4218 */  
	void (*Trigger_control_Tick)(void);
	/* Read functions */
	/* @generated_from _2022x_1_1ad00ce_1677833598836_377885_3507 */
	/* @generated_from _2022x_1_1ad00ce_1679479084488_911966_6781 */  
	void (*Read_pin_io)(Boolean* msg);
	/* Server call functions */
	/* pointer to user section */
#ifdef __cplusplus
	void *user_section;
#else
	struct Button *user_section;
#endif
}Cse_Button_Struct;

/* Runnable functions declaration--------------- */
/* @generated_from _2022x_1ad00ce_1659686618091_973487_3644 */ 
void Button_Button(Cse_Button_Struct* instance);
/* @generated_from _2022x_1ad00ce_1659686618091_973487_3644 */ 
void Button_Button_init(Cse_Button_Struct* instance);
/* @generated_from _2022x_1ad00ce_1659686618091_973487_3644 */ 
void Button_Button_end(Cse_Button_Struct* instance);

#ifndef CSE_C

/* Runnable API Mapping -------------------------*/
/* @generated_from _2022x_1ad00ce_1659686618091_973487_3644 */ 
#define Button(...) Button_Button(__VA_ARGS__)
/* @generated_from _2022x_1ad00ce_1659686618091_973487_3644 */ 
#define Button_init(...) Button_Button_init(__VA_ARGS__)
/* @generated_from _2022x_1ad00ce_1659686618091_973487_3644 */ 
#define Button_end(...) Button_Button_end(__VA_ARGS__)

#ifdef CSE_CONTRACT
/* CSE Contract for documentation purpose -------*/
/* Write functions declaration */
void Cse_Write_control_snd(Cse_Button_Struct* instance, Boolean* msg);

/* Trigger functions declaration */
void Cse_Trigger_control_On(Cse_Button_Struct* instance);
void Cse_Trigger_control_Off(Cse_Button_Struct* instance);
void Cse_Trigger_control_Tick(Cse_Button_Struct* instance);

/* Read functions declaration */
void Cse_Read_pin_io(Cse_Button_Struct* instance, Boolean* msg);

/* Server call functions declaration */

#else /* #ifdef CSE_CONTRACT */
/* CSE API Mapping used at compilation phase ----*/
#define Cse_Write_control_snd(inst, data) (inst)->Write_control_snd(data)
#define Cse_Trigger_control_On(inst) (inst)->Trigger_control_On()
#define Cse_Trigger_control_Off(inst) (inst)->Trigger_control_Off()
#define Cse_Trigger_control_Tick(inst) (inst)->Trigger_control_Tick()
#define Cse_Read_pin_io(inst, data) (inst)->Read_pin_io(data)

#endif /* #ifdef CSE_CONTRACT */

#endif /* #ifndef CSE_C*/

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* Button_ITF_H */

/* end of file */
