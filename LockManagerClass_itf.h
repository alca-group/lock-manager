/* @date 2024-06-04T08:56:16.910912581Z */

/**********************************************************************
 * @file LockManagerClass_itf.h
 *
 * Automatically generated by Software Producer Engineer
 * Runtime code generator @version 
 * @generated_from _2024x_16600ad_1696420824973_365327_18510
 **********************************************************************/

#ifndef CSE_LockManagerClass_ITF_H
#define CSE_LockManagerClass_ITF_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Includes -------------------------------------*/
#include "LockManagerClass_type.h"

#include "Cse_cfg.h" /* define NB_INST_LOCKMANAGERCLASS */

/* CSE Component Structure declaration ----------*/
typedef struct{
	/* Write functions */
	/* @generated_from _2024x_16600ad_1696420860361_111713_18781 */
	/* @generated_from _2024x_16600ad_1696519225321_337506_19773 */	
	void (*Write_output_lockStatus)(LockEnum* msg);
	/* Trigger functions */
	/* Read functions */
	/* @generated_from _2024x_16600ad_1696420852093_406377_18753 */
	/* @generated_from _2024x_16600ad_1696519491118_49138_19920 */  
	void (*Read_input_lockControl)(ControlEnum* msg);
	/* Server call functions */
	/* pointer to user section */
#ifdef __cplusplus
	void *user_section;
#else
	struct LockManagerClass *user_section;
#endif
}Cse_LockManagerClass_Struct;

/* Runnable functions declaration--------------- */
/* @generated_from _2024x_16600ad_1696420841228_369349_18521 */ 
void LockManagerClass_Statemachine(Cse_LockManagerClass_Struct* instance);
/* @generated_from _2024x_16600ad_1696420841228_369349_18521 */ 
void LockManagerClass_Statemachine_init(Cse_LockManagerClass_Struct* instance);
/* @generated_from _2024x_16600ad_1696420841228_369349_18521 */ 
void LockManagerClass_Statemachine_end(Cse_LockManagerClass_Struct* instance);

#ifndef CSE_C

/* Runnable API Mapping -------------------------*/
/* @generated_from _2024x_16600ad_1696420841228_369349_18521 */ 
#define Statemachine(...) LockManagerClass_Statemachine(__VA_ARGS__)
/* @generated_from _2024x_16600ad_1696420841228_369349_18521 */ 
#define Statemachine_init(...) LockManagerClass_Statemachine_init(__VA_ARGS__)
/* @generated_from _2024x_16600ad_1696420841228_369349_18521 */ 
#define Statemachine_end(...) LockManagerClass_Statemachine_end(__VA_ARGS__)

#ifdef CSE_CONTRACT
/* CSE Contract for documentation purpose -------*/
/* Write functions declaration */
void Cse_Write_output_lockStatus(Cse_LockManagerClass_Struct* instance, LockEnum* msg);

/* Trigger functions declaration */

/* Read functions declaration */
void Cse_Read_input_lockControl(Cse_LockManagerClass_Struct* instance, ControlEnum* msg);

/* Server call functions declaration */

#else /* #ifdef CSE_CONTRACT */
/* CSE API Mapping used at compilation phase ----*/
#define Cse_Write_output_lockStatus(inst, data) (inst)->Write_output_lockStatus(data)
#define Cse_Read_input_lockControl(inst, data) (inst)->Read_input_lockControl(data)

#endif /* #ifdef CSE_CONTRACT */

#endif /* #ifndef CSE_C*/

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* LockManagerClass_ITF_H */

/* end of file */
