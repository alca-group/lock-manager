/* @date 2024-06-04T08:56:16.852667117Z */

/**********************************************************************
 * @file Cse_LockManagerClass_hook_usr.c
 *
 * Automatically generated by Software Producer Engineer
 * Runtime code generator @version 1.0.0
 **********************************************************************/

/* define symbol USER_HOOKS to enable this file content */
#ifdef USER_HOOKS

#include "Cse_cfg.h"

/**
 * Function Cse_ReadHook_LockManagerClass_input_lockControl_Start
 * User specific function to implement specific behavior
 * @generated_from ${class_generated_from_id}
 */
void Cse_ReadHook_LockManagerClass_input_lockControl_Start(ControlEnum* from_UNCONNECTED_input_lockControl){
	/* BEG_USER_CODE(${id}) do not remove */
	/* Add your includes here */
	/* END_USER_CODE(${id}) do not remove */
}

/**
 * Function Cse_ReadHook_LockManagerClass_input_lockControl_Return
 * User specific function to implement specific behavior
 * @generated_from ${class_generated_from_id}
 */
void Cse_ReadHook_LockManagerClass_input_lockControl_Return(ControlEnum* from_UNCONNECTED_input_lockControl){
	/* BEG_USER_CODE(${id}) do not remove */
	/* Add your includes here */
	/* END_USER_CODE(${id}) do not remove */
}

/**
 * Function Cse_WriteHook_LockManagerClass_output_lockStatus_Start
 * User specific function to implement specific behavior
 * @generated_from ${class_generated_from_id}
 */
void Cse_WriteHook_LockManagerClass_output_lockStatus_Start(LockEnum* from_UNCONNECTED_output_lockStatus){
	/* BEG_USER_CODE(${id}) do not remove */
	/* Add your includes here */
	/* END_USER_CODE(${id}) do not remove */
}

/**
 * Function Cse_WriteHook_LockManagerClass_output_lockStatus_Return
 * User specific function to implement specific behavior
 * @generated_from ${class_generated_from_id}
 */
void Cse_WriteHook_LockManagerClass_output_lockStatus_Return(LockEnum* from_UNCONNECTED_output_lockStatus){
	/* BEG_USER_CODE(${id}) do not remove */
	/* Add your includes here */
	/* END_USER_CODE(${id}) do not remove */
}


#endif // #ifdef USER_HOOKS
/* end of file */
